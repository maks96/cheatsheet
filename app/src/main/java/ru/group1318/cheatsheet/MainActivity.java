package ru.group1318.cheatsheet;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import ru.group1318.cheatsheet.fragment.FragmentArray;
import ru.group1318.cheatsheet.fragment.FragmentBoolean;
import ru.group1318.cheatsheet.fragment.FragmentFloatDouble;
import ru.group1318.cheatsheet.fragment.FragmentInt;
import ru.group1318.cheatsheet.fragment.FragmentString;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentString fstring;
    FragmentArray farray;
    FragmentFloatDouble ffloat;
    FragmentInt fint;
    FragmentBoolean fboolean;
    TextView init_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init_text = (TextView) findViewById(R.id.textInit);
        init_text.setText(R.string.init_message);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fstring = new FragmentString();
        farray = new FragmentArray();
        ffloat = new FragmentFloatDouble();
        fint = new FragmentInt();
        fboolean = new FragmentBoolean();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentTransaction ftrans = getSupportFragmentManager().beginTransaction();

        if (id == R.id.type_int) {
            ftrans.replace(R.id.container, fint);
        } else if (id == R.id.type_double) {
            ftrans.replace(R.id.container, ffloat);
        } else if (id == R.id.type_boolean) {
            ftrans.replace(R.id.container, fboolean);
        } else if (id == R.id.type_string) {
            ftrans.replace(R.id.container, fstring);
        } else if (id == R.id.type_array) {
            ftrans.replace(R.id.container, farray);
        }
        init_text.setText("");
        setTitle(item.getTitle());
        ftrans.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

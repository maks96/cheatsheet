package ru.group1318.cheatsheet.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.group1318.cheatsheet.InputFile;
import ru.group1318.cheatsheet.R;

public class FragmentInt extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        TextView textView = new TextView(getActivity());
        InputFile file = new InputFile(getActivity().getApplicationContext(), "type_int");
        textView.setText(file.readFile());
        textView.setTextSize(20);
        textView.setMovementMethod(new ScrollingMovementMethod());

        return textView;

    }
}

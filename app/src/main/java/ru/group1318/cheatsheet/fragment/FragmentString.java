package ru.group1318.cheatsheet.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.group1318.cheatsheet.InputFile;
import ru.group1318.cheatsheet.R;

public class FragmentString extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TextView textView = new TextView(getActivity());
        InputFile file = new InputFile(getActivity().getApplicationContext(), "type_array");
        textView.setText(file.readFile());
        textView.setTextSize(20);
        textView.setMovementMethod(new ScrollingMovementMethod());
        return textView;
    }
}
